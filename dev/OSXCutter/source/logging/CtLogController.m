//
//  CtLogController.m
//  OSXCutter
//
//  Created by Thomas Bonk on 09.10.07.
//  Copyright 2007 Thomas Bonk. All rights reserved.
//  
//  Creative Commons
//  Attribution-Noncommercial-Share Alike 2.0 Germany
//  You are free:
//      * to Share — to copy, distribute and transmit the work
//      * to Remix — to adapt the work
//  
//  Under the following conditions:
//  
//      * Attribution. You must attribute the work in the manner specified by 
//        the author or licensor (but not in any way that suggests that they 
//        endorse you or your use of the work).
//      * Noncommercial. You may not use this work for commercial purposes.
//      * Share Alike. If you alter, transform, or build upon this work, you may
//        distribute the resulting work only under the same or similar license 
//        to this one.
//      * For any reuse or distribution, you must make clear to others the 
//        license terms of this work. The best way to do this is with a link to 
//        this web page.
//      * Any of the above conditions can be waived if you get permission from 
//        the copyright holder.
//      * Nothing in this license impairs or restricts the author's moral rights.
//  
//  
//  Your fair dealing and other rights are in no way affected by the above.
//  This is a human-readable summary of the Legal Code (the full license):
//  http://creativecommons.org/licenses/by-nc-sa/2.0/de/legalcode
//


#import <CocoLogger/CLNotificationAppender.h>
#import "CtLogController.h"


@interface CtLogController (private)

- (void)logMessage:(NSNotification*)theNotification;

@end



@implementation CtLogController

#pragma mark Initialization / Deallocation

- (id)init {

	if (self = [super init]) {
		
		[[NSNotificationCenter defaultCenter] addObserver:self 
												 selector:@selector(logMessage:)
													 name:CLAppenderNotificationName 
												   object:nil];
	}
	
	return self;
}

- (void)dealloc {

	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	[super dealloc];
}


#pragma mark Private messages

- (void)logMessage:(NSNotification*)theNotification {

	NSDictionary* userInfo    = [theNotification userInfo];
	NSString*     appenderKey = [userInfo objectForKey:CLNotificationAppenderKey];
	NSString*     message     = [userInfo objectForKey:CLNotificationAppenderMessage];
	
	if ([appenderKey isEqualToString:@"OSXCutter"]) {
		
		NSRange range;
		
		range.location = [[_textView textStorage] length];
		range.length = 0;
		[_textView replaceCharactersInRange:range withString:message];
		range.length = [message length];
		[_textView scrollRangeToVisible:range];
		
		NSLog(@"%@", message);
	}
}

@end
