//
//  CtMovie.h
//  OSXCutter
//
//  Created by Thomas Bonk on 07.06.07.
//  Copyright 2007 Thomas Bonk. All rights reserved.
//  
//  Creative Commons
//  Attribution-Noncommercial-Share Alike 2.0 Germany
//  You are free:
//      * to Share — to copy, distribute and transmit the work
//      * to Remix — to adapt the work
//  
//  Under the following conditions:
//  
//      * Attribution. You must attribute the work in the manner specified by 
//        the author or licensor (but not in any way that suggests that they 
//        endorse you or your use of the work).
//      * Noncommercial. You may not use this work for commercial purposes.
//      * Share Alike. If you alter, transform, or build upon this work, you may
//        distribute the resulting work only under the same or similar license 
//        to this one.
//      * For any reuse or distribution, you must make clear to others the 
//        license terms of this work. The best way to do this is with a link to 
//        this web page.
//      * Any of the above conditions can be waived if you get permission from 
//        the copyright holder.
//      * Nothing in this license impairs or restricts the author's moral rights.
//  
//  
//  Your fair dealing and other rights are in no way affected by the above.
//  This is a human-readable summary of the Legal Code (the full license):
//  http://creativecommons.org/licenses/by-nc-sa/2.0/de/legalcode
//


#import <Cocoa/Cocoa.h>
#import <QTKit/QTKit.h>
#import "CtProgressProtocol.h"


typedef enum {
	
	NoError,
	NotAMovie,
	DecodeError,
	NoCutlists,
	ErrorLoadingCutlist
} CtMovieStatus;


typedef enum {
	
	CtMovieFileTypeUnknown,
	CtMovieFileTypeOtrKey,
	CtMovieFileTypeAvi,
	CtMovieFileTypeHqAvi,
	CtMovieFileTypeMp4
} CtMovieFileType;


@interface CtMovie : NSObject {

	NSString*       _qualifiedFilename;
	NSString*       _filename;
	long            _filesize;
	NSMutableArray* _cutlists;
	NSString*       _targetPath;
	QTMovie*        _movie;
	CtMovieStatus   _status;
	BOOL            _fileDeleted;
}

#pragma mark Construction/Destruction

- (id)init;
- (id)initWithQualifiedFilename:(NSString*)theQualifiedFilename;
- (void)dealloc;


#pragma mark Accessors for the attributes

- (NSString*)qualifiedFilename;
- (void)setQualifiedFilename:(NSString*)theQualifiedFilename;
- (NSString*)filename;
- (long)filesize;
- (NSArray*)cutlists;
- (NSString*)targetPath;
- (void)setTargetPath:(NSString*)value;
- (CtMovieStatus)status;
- (void)setStatus:(CtMovieStatus)value;
- (CtMovieFileType)movieFileType;
- (BOOL)fileDeleted;


#pragma mark Loading local cutlists

- (void)loadCutlistsFromFiles:(NSArray*)filenames;


#pragma mark Utility methods

- (BOOL)loadMovie;
- (void)loadCutlistInfo;
- (void)performCutAndTrigger:(id<CtProgressProtocol>)aTarget;

@end
