//
//  CtMovie.m
//  OSXCutter
//
//  Created by Thomas Bonk on 07.06.07.
//  Copyright 2007 Thomas Bonk. All rights reserved.
//  
//  Creative Commons
//  Attribution-Noncommercial-Share Alike 2.0 Germany
//  You are free:
//      * to Share — to copy, distribute and transmit the work
//      * to Remix — to adapt the work
//  
//  Under the following conditions:
//  
//      * Attribution. You must attribute the work in the manner specified by 
//        the author or licensor (but not in any way that suggests that they 
//        endorse you or your use of the work).
//      * Noncommercial. You may not use this work for commercial purposes.
//      * Share Alike. If you alter, transform, or build upon this work, you may
//        distribute the resulting work only under the same or similar license 
//        to this one.
//      * For any reuse or distribution, you must make clear to others the 
//        license terms of this work. The best way to do this is with a link to 
//        this web page.
//      * Any of the above conditions can be waived if you get permission from 
//        the copyright holder.
//      * Nothing in this license impairs or restricts the author's moral rights.
//  
//  
//  Your fair dealing and other rights are in no way affected by the above.
//  This is a human-readable summary of the Legal Code (the full license):
//  http://creativecommons.org/licenses/by-nc-sa/2.0/de/legalcode
//


#import <QTKit/QTKit.h>
#import "CtCommon.h"
#import "CtPreferencesModel.h"
#import "CtApplicationController.h"
#import "CtMovie.h"
#import "CtCutlist.h"


@interface CtMovie (private)

#pragma mark Utility Methods

- (CtCutlist*)cutlistForCutting;
- (CtCutlist*)selectedCutlist;

@end


@implementation CtMovie

#pragma mark Construction/Destruction

- (id)init {
	
	if (self = [super init]) {
		
		_qualifiedFilename = nil;
		_filename = nil;
		_filesize = -1;
		_cutlists = [[NSMutableArray alloc] init];
		_movie = nil;
		_fileDeleted = NO;
	}
	
	return self;
}

- (id)initWithQualifiedFilename:(NSString*)theQualifiedFilename {
	
	if (self = [super init]) {
		
		_qualifiedFilename = [theQualifiedFilename retain];
		_filename = [[_qualifiedFilename lastPathComponent] retain];
		_filesize = -1;
		_cutlists = [[NSMutableArray alloc] init];
		_status = NoError;
		_movie = nil;
		_fileDeleted = NO;
	}
	
	return self;
}

- (void)dealloc {

	[_qualifiedFilename release];
	_qualifiedFilename = nil;
	
	[_filename release];
	_filename = nil;
	
	[_cutlists release];
	_cutlists = nil;
	
	[_movie release];
	_movie = nil;
	
	[super dealloc];
}


#pragma mark Accessors for the attributes

- (NSString*)qualifiedFilename {
	
	return _qualifiedFilename;	
}

- (void)setQualifiedFilename:(NSString*)theQualifiedFilename {
	
	if (_qualifiedFilename != theQualifiedFilename) {
	
		[theQualifiedFilename retain];
		[_qualifiedFilename release];
		_qualifiedFilename = theQualifiedFilename;
		
		if (_qualifiedFilename != nil) {
			
			_filename = [[_qualifiedFilename lastPathComponent] retain];
		}
		
		_filesize = -1;
	}
}

- (NSString*)filename {
	
	return _filename;
}

- (long)filesize {
	
	if (_filesize == -1 && [self qualifiedFilename] != nil) {
		
		NSFileManager *fileManager = [NSFileManager defaultManager];
		NSDictionary  *fileAttributes = [fileManager fileAttributesAtPath:[self qualifiedFilename]
															 traverseLink:YES];
	
		if (fileAttributes != nil) {
			
			NSNumber *fileSize;
			
			if (fileSize = [fileAttributes objectForKey:NSFileSize]) {
				
				_filesize = [fileSize longValue];
			}
		}
		else {
			
			_filesize = -1;
		}
	}
	
	return _filesize;
}

- (NSArray*)cutlists {
	
	if ([_cutlists count] == 0) {
		
		[self loadCutlistInfo];
	}
	
	return (NSArray*)_cutlists;
}

- (NSString*)targetPath {
	
    return _targetPath;
}

- (void)setTargetPath:(NSString*)value {
	
    if (_targetPath != value) {
		
		[value retain];
        [_targetPath release];
        _targetPath = value;
    }
}

- (CtMovieStatus)status {
	
    return _status;
}

- (void)setStatus:(CtMovieStatus)value {

	_status = value;
}

- (CtMovieFileType)movieFileType {
	
	CtMovieFileType fileType = CtMovieFileTypeUnknown;
	NSString*       fn       = [[self filename] uppercaseString];
	
	if ([fn hasSuffix:@".MP4"]) {
		
		fileType = CtMovieFileTypeMp4;
	}
	else if ([fn hasSuffix:@".HQ.AVI"]) {
		
		fileType = CtMovieFileTypeHqAvi;
	}
	else if ([fn hasSuffix:@".AVI"]) {
	
		fileType = CtMovieFileTypeAvi;
	}
	else if ([fn hasSuffix:@".OTRKEY"]) {
		
		fileType = CtMovieFileTypeOtrKey;
	}
	
	return fileType;
}

- (BOOL)fileDeleted {
	
	return _fileDeleted;
}



#pragma mark Loading local cutlists

- (void)loadCutlistsFromFiles:(NSArray*)filenames {

	if ([self status] != NotAMovie) {
		
		NSEnumerator* enu           = [filenames objectEnumerator];
		BOOL          cutlistLoaded = NO;
		NSString*     filename;
		
		while (filename = [enu nextObject]) {
			
			CtCutlist* cutlist = [CtCutlist cutlistWithContentsOfFile:filename];
			
			if (cutlist != nil) {
			
				if ([[self filename] hasPrefix:[cutlist movieName]]) {
					
					[_cutlists addObject:cutlist];
					cutlistLoaded = YES;
				}
			}
		}
		
		if (cutlistLoaded) {
			
			[self setStatus:NoError];
			[_cutlists makeObjectsPerformSelector:@selector(deselect)];
			[_cutlists sortUsingSelector:@selector(compareRating:)];
			if ([_cutlists count] > 0) {
				
				[[_cutlists objectAtIndex:0] select];
			}
		}
	}
}




#pragma mark Utility methods

- (BOOL)loadMovie {
	
	NSError* error = nil;
	
	if (_movie != nil) {
		
		[_movie release];
		_movie = nil;
	}
	
	_movie = [[QTMovie alloc] initWithFile:[self qualifiedFilename] error:&error];
	
	if (error != nil) {
		
		[self setStatus:NotAMovie];
		[_movie release];
		
		[CtLogger error:@"Error while loading movie: %@. Error = %@", [self qualifiedFilename], error];
	}
	
	return (error == nil);
}

- (CtCutlist*)cutlistForCutting {
	
	CtCutlist* cutlist = [self selectedCutlist];
	
	if (cutlist == nil && [[self cutlists] count] > 0) {
		
		cutlist = [[self cutlists] objectAtIndex:0];
	}
	
	return cutlist;
}

- (CtCutlist*)selectedCutlist {

	if ([[self cutlists] count] > 0) {
		
		NSEnumerator* enu     = [[self cutlists] objectEnumerator];
		CtCutlist*    cutlist;
		
		while (cutlist = [enu nextObject]) {
		
			if ([cutlist selected]) {
				
				return cutlist;
			}
		}
	}
	
	return nil;
}

- (NSDictionary*)exportAttributes {
	
	CtPreferencesModel* prefs = [CtPreferencesModel instance];
	CtMovieFileType     type  = [self movieFileType];
	NSDictionary*       sets  = nil;
	
	if (   (type == CtMovieFileTypeAvi && [prefs exportExtAviToAvi])
		|| (type == CtMovieFileTypeHqAvi && [prefs exportExtHqAviToAvi])) {
		
		sets = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], QTMovieExport, 
			                                              [NSNumber numberWithLong:kQTFileTypeAVI], QTMovieExportType, nil];
	}
	else if (type == CtMovieFileTypeMp4 && [prefs exportExtMp4ToMp4]) {
		
		sets = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], QTMovieExport, 
			                                              [NSNumber numberWithLong:kQTFileTypeMP4], QTMovieExportType, nil];
	}
	else {
		
		sets = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] 
									       forKey:QTMovieFlatten];
	}
	
	return sets;
}

- (NSString*)exportFilename {
	
	CtPreferencesModel* prefs = [CtPreferencesModel instance];
	CtMovieFileType     type  = [self movieFileType];
	NSString*           name  = nil;
	
	if (   (type == CtMovieFileTypeAvi && [prefs exportExtAviToAvi])
		|| (type == CtMovieFileTypeHqAvi && [prefs exportExtHqAviToAvi])
	    || (type == CtMovieFileTypeMp4 && [prefs exportExtMp4ToMp4])) {
		
		name = [NSString stringWithFormat:@"%@/%@", [self targetPath], [self filename]];
	}
	else {
		
		name = [NSString stringWithFormat:@"%@/%@.mov", [self targetPath], [self filename]];
	}
	
	return name;
}

- (void)moveOriginalFileToTrash {

	int       tag;
	NSString* file = [self qualifiedFilename];
	
	[_movie release];
	_movie = nil;
	
	_fileDeleted = [[NSWorkspace sharedWorkspace] performFileOperation:NSWorkspaceRecycleOperation
																source:[file stringByDeletingLastPathComponent]
														   destination:@""
																 files:[NSArray arrayWithObject:[file lastPathComponent]]
																   tag:&tag];
}

- (void)performCutAndTrigger:(id<CtProgressProtocol>)aTarget {
	
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	
	if ([aTarget progressingMovie:self]) {
			
		if ([[self cutlists] count] > 0) {
		
			NSError*   error   = nil;
			
			if (error == nil) {
				
				NSError*   error   = nil;
				CtCutlist* cutlist = [self cutlistForCutting];
				
				[aTarget setCurrentCuttingStep:@"Loading cutlist"];
				[cutlist loadCutlistData:&error];
				
				if (error == nil) {

					QTMovie* cutMovie   = [QTMovie movie];

					[cutMovie setAttribute:[NSNumber numberWithBool:YES] forKey:QTMovieEditableAttribute];
									
					if (error == nil) {
						
						if ([cutlist cuts] > 0) {
							
							long scale = [[_movie attributeForKey:QTMovieTimeScaleAttribute] longValue];
						
							[aTarget setMinCuttingProgress:0];
							[aTarget setMaxCuttingProgress:[cutlist cuts]];
							[aTarget setCurrentCuttingStep:@"Cutting Movie"];
						
							for (int i = 0, n = [cutlist cuts]; i < n; i++) {
					
								double      start    = 0.0;
								double      duration = 0.0;
								QTTime      qtStart;
								QTTime      qtCut;
								QTTimeRange qtSelection;
							
								[aTarget setCurrentProgress:(double)i];
						
								[cutlist startTimestamp:&start andDuration:&duration ofCut:i];
							
								qtStart = QTMakeTime( start * scale, scale );
								qtCut   = QTMakeTime( duration * scale, scale );
								qtSelection = QTMakeTimeRange( qtStart, qtCut );
							
								[_movie setSelection:qtSelection];
								[cutMovie appendSelectionFromMovie:_movie];
							
								if (![aTarget progressingMovie:self]) {
								
									break;
								}
							}
						
							if ([aTarget progressingMovie:self]) {
												
								[aTarget setCurrentCuttingStep:@"Saving Movie"];
							
								[cutMovie setDelegate:aTarget];
								[aTarget setMinCuttingProgress:0];
								[aTarget setMaxCuttingProgress:100];
							
								[cutMovie writeToFile:[self exportFilename]
									   withAttributes:[self exportAttributes]]; 
							
								[cutMovie setDelegate:nil];
								
								if ([aTarget progressingMovie:self]) {
									
									if ([[CtPreferencesModel instance] deleteOriginalFilesAfterCut]) {
										
										[self moveOriginalFileToTrash];
									}
								}
							}
						}
						else {
						
							[aTarget progressingMovie:self];
							[CtLogger warn:@"The cutlist %d for the movie %@ doesn't have any cuts.", [cutlist identifier], [self qualifiedFilename]];
						}
					}
				}
				else {
			
					[self setStatus:ErrorLoadingCutlist];
					[aTarget progressingMovie:self];
					[CtLogger error:@"Error while loading data for cutlist %d. Error = %@", [cutlist identifier], error];
				}
			}
		}
	}
	
	[aTarget fireFinishedCuttingMovie:self];
	[pool release];
}

- (void)loadCutlistInfo {

	if( [self status] != NotAMovie && [[CtPreferencesModel instance] loadCutlistsFromServer]) {
		
		NSError*       error        = nil;
		NSString*      urlString    = [NSString stringWithFormat:@"%@/getxml.php?ofsb=%d&version=0.9.9.5",[[CtPreferencesModel instance] cutlistServer] ,[self filesize]];
		NSURL*         url          = [NSURL URLWithString:urlString];		
		NSURLRequest*  request   = [NSURLRequest requestWithURL:url 
													cachePolicy:NSURLRequestReloadIgnoringCacheData 
												timeoutInterval:[[CtPreferencesModel instance] networkTimeout]];
		NSURLResponse* response  = nil;
		NSData*        data      = [NSURLConnection sendSynchronousRequest:request 
														 returningResponse:&response 
																	 error:&error];
		NSXMLDocument* cutlistsInfo = nil;
																
		if (error == nil) {
		
			cutlistsInfo = [[NSXMLDocument alloc] initWithData:data 
												       options:NSXMLDocumentTidyXML 
													     error:&error];
		}
		
		if (error == nil) {
			
			NSArray* cutlistNodes    = [cutlistsInfo nodesForXPath:@"//files/node()" error:&error];
			
			if (error == nil && [cutlistNodes count] > 0) {
				
				for (int i = 0, n = [cutlistNodes count]; i < n; i++) {
					
					NSXMLElement* obj = [cutlistNodes objectAtIndex:i];
					NSArray* ids             = [obj nodesForXPath:[NSString stringWithFormat:@"//cutlist[%d]/id/text()",i+1] error:&error];
					NSArray* names           = [obj nodesForXPath:[NSString stringWithFormat:@"//cutlist[%d]/name/text()",i+1] error:&error];
					NSArray* ratings         = [obj nodesForXPath:[NSString stringWithFormat:@"//cutlist[%d]/rating/text()",i+1] error:&error];
					NSArray* ratingCounts    = [obj nodesForXPath:[NSString stringWithFormat:@"//cutlist[%d]/ratingcount/text()",i+1] error:&error];
					NSArray* authors         = [obj nodesForXPath:[NSString stringWithFormat:@"//cutlist[%d]/author/text()",i+1] error:&error];
					NSArray* ratingByAuthors = [obj nodesForXPath:[NSString stringWithFormat:@"//cutlist[%d]/ratingbyauthor/text()",i+1] error:&error];
					NSArray* actualContents  = [obj nodesForXPath:[NSString stringWithFormat:@"//cutlist[%d]/actualcontent/text()",i+1] error:&error];
					NSArray* userComments    = [obj nodesForXPath:[NSString stringWithFormat:@"//cutlist[%d]/usercomment/text()",i+1] error:&error];			 				
					int		  theId			    = 0;
					NSString* theName		    = @"";
					float     theRating         = 0.0;
					int       theRatingCount    = 0;
					NSString* theAuthor         = @"";
					int       theRatingByAuthor = 0;
					NSString* theActualContent  = @"";
					NSString* theUserComment    = @"";

					if (error == nil && [ids count] > 0) {
						
						theId = [[[ids objectAtIndex:0] stringValue] intValue];
					}
					
					if (theId > 0)
					{
					
						if ([names count] > 0) {
						
							theName = [[names objectAtIndex:0] stringValue];
						}
					
						if ([ratings count] > 0) {
						
							theRating = [[[ratings objectAtIndex:0] stringValue] floatValue];
						}
					
						if ([ratingCounts count] > 0) {
						
							theRatingCount = [[[ratingCounts objectAtIndex:0] stringValue] intValue];
						}
					
						if ([authors count] > 0) {
						
							theAuthor = [[authors objectAtIndex:0] stringValue];
						}
					
						if ([ratingByAuthors count] > 0) {
						
							theRatingByAuthor = [[[ratingByAuthors objectAtIndex:0] stringValue] intValue];
						}
						
						if (theRating == 0.0 && theRatingCount == 0) {
							
							theRating = theRatingByAuthor;
							theRatingCount = 1;
						}
					
						if ([actualContents count] > 0) {
						
							theActualContent = [[actualContents objectAtIndex:0] stringValue];
						}
					
						if ([userComments count] > 0) {
						
							theUserComment = [[userComments objectAtIndex:0] stringValue];
						}
						
						CtCutlist* cutlist = [CtCutlist cutlist];
						
						[cutlist setIdentifier:theId];
						[cutlist setName:theName];
						[cutlist setRating:theRating];
						[cutlist setRatingCount:theRatingCount];
						[cutlist setAuthor:theAuthor];
						[cutlist setRatingByAuthor:theRatingByAuthor];
						[cutlist setActualContent:theActualContent];
						[cutlist setUserComment:theUserComment];
						
						[_cutlists addObject:cutlist];
					}
					
					[_cutlists sortUsingSelector:@selector(compareRating:)];
				}
			}
			
			if ([_cutlists count] > 0) {
				
				[[_cutlists objectAtIndex:0] select];
			}
			
			[self setStatus:NoError];
		}
		else {
			
			[self setStatus:NoCutlists];
			[CtLogger error:@"Error while loading cutlist info. Error = %@", error];
		}
	}
}

@end
