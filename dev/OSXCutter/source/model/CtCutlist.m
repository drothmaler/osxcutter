//
//  CtCutlist.m
//  OSXCutter
//
//  Created by Thomas Bonk on 08.06.07.
//  Copyright 2007 Thomas Bonk. All rights reserved.
//  
//  Creative Commons
//  Attribution-Noncommercial-Share Alike 2.0 Germany
//  You are free:
//      * to Share — to copy, distribute and transmit the work
//      * to Remix — to adapt the work
//  
//  Under the following conditions:
//  
//      * Attribution. You must attribute the work in the manner specified by 
//        the author or licensor (but not in any way that suggests that they 
//        endorse you or your use of the work).
//      * Noncommercial. You may not use this work for commercial purposes.
//      * Share Alike. If you alter, transform, or build upon this work, you may
//        distribute the resulting work only under the same or similar license 
//        to this one.
//      * For any reuse or distribution, you must make clear to others the 
//        license terms of this work. The best way to do this is with a link to 
//        this web page.
//      * Any of the above conditions can be waived if you get permission from 
//        the copyright holder.
//      * Nothing in this license impairs or restricts the author's moral rights.
//  
//  
//  Your fair dealing and other rights are in no way affected by the above.
//  This is a human-readable summary of the Legal Code (the full license):
//  http://creativecommons.org/licenses/by-nc-sa/2.0/de/legalcode
//


#import <CocoFoundation/CFIniProperties.h>
#import "CtCutlist.h"
#import "CtPreferencesModel.h"


@implementation CtCutlist

#pragma mark Construction/Destruction

+ (CtCutlist*)cutlist {

	CtCutlist* cutlist = [[CtCutlist alloc] init];
	
	return [cutlist autorelease];
}

+ (CtCutlist*)cutlistWithContentsOfFile:(NSString*)filename {

	CtCutlist* cutlist = [[CtCutlist alloc] initWithContentsOfFile:filename];
	
	return [cutlist autorelease];
}

- (id)init {

	if (self = [super init]) {
		
		_identifier = 0;
		_name = nil;
		_rating = 0.0;
		_ratingCount = 0;
		_author = nil;
		_ratingByAuthor = 0;
		_actualContent = nil;
		_userComment = nil;
		_properties = nil;
	}
	
	return self;
}

- (id)initWithContentsOfFile:(NSString*)filename {

	if (self = [super init]) {
		
		_identifier = CtManualCutlistID;
		_name = [[filename lastPathComponent] retain];
		_rating = 0.0;
		_ratingCount = 0;
		_author = nil;
		_ratingByAuthor = 0;
		_actualContent = nil;
		_userComment = nil;
		_properties = nil;
		
		@try {
			
			NSData*   data           = [NSData dataWithContentsOfFile:filename];
			NSString* author         = nil;
			int       ratingByAuthor = 0;
			NSString* actualContent  = nil;
			NSString* userComment    = nil;
			
			_properties = [[CFIniProperties alloc] initWithData:data];
			
			if ([_properties stringValue:&author forKey:@"Author" ofSection:@"Info"]) {
				
				[self setAuthor:author];
			}
			
			if ([_properties intValue:&ratingByAuthor forKey:@"RatingByAuthor" ofSection:@"Info"]) {
				
				[self setRatingByAuthor:ratingByAuthor];
				[self setRating:ratingByAuthor];
				[self setRatingCount:1];
			}
			
			if ([_properties stringValue:&actualContent forKey:@"ActualContent" ofSection:@"Info"]) {
				
				[self setActualContent:actualContent];
			}
			
			if ([_properties stringValue:&userComment forKey:@"UserComment" ofSection:@"Info"]) {
				
				[self setUserComment:userComment];
			}
		}
		@catch (NSException* ex) {
			
			[self dealloc];
			return nil;
		}
	}
	
	return self;
}

- (void)dealloc {
	
	[_name release];
	_name = nil;
	
	[_author release];
	_author = nil;
	
	[_actualContent release];
	_actualContent = nil;
	
	[_userComment release];
	_userComment = nil;
	
	[_properties release];
	_properties = nil;
	
	[super dealloc];
}


#pragma mark Utility methods

- (NSComparisonResult)compareRating:(CtCutlist*)otherCutlist {

	float myRating    = [self rating] == 0.0 && [self ratingCount] == 0 ? [self ratingByAuthor] : [self rating];
	float otherRating = [otherCutlist rating] == 0.0 && [otherCutlist ratingCount] == 0 ? [otherCutlist ratingByAuthor] : [otherCutlist rating];
	
	if (myRating == otherRating) {
		
		return NSOrderedSame;
	}
	else if (myRating < otherRating) {
		
		return NSOrderedDescending;
	}
	else {
		
		return NSOrderedAscending;
	}
}

- (void)loadCutlistData:(NSError**)error {

	*error = nil;
	
	if (_properties == nil) {
	
		NSString*      urlString = [NSString stringWithFormat:@"%@/getfile.php?id=%d", [[CtPreferencesModel instance] cutlistServer], [self identifier]];
		NSURL*         url       = [NSURL URLWithString:urlString];
		NSURLRequest*  request   = [NSURLRequest requestWithURL:url 
													cachePolicy:NSURLRequestReloadIgnoringCacheData 
												timeoutInterval:[[CtPreferencesModel instance] networkTimeout]];
		NSURLResponse* response  = nil;
		NSData*        data      = [NSURLConnection sendSynchronousRequest:request 
														 returningResponse:&response 
																	 error:error];
		
		if (*error == nil) {
			
			@try {
				
				_properties = [[CFIniProperties alloc] initWithData:data];
			}
			@catch (NSException* ex) {
			
			}
		}
	}
}


#pragma mark Accessors for special attributes

- (NSString*)movieName {
	
	NSString* movieName = nil;
	
	if (_properties != nil) {
		
		if ([_properties stringValue:&movieName forKey:@"ApplyToFile" ofSection:@"General"]) {
			
			NSArray* parts = [movieName componentsSeparatedByString:@"."];
			
			movieName = @"";
			
			for (int i = 0; i < 3; i++) {
				
				if ([movieName length] > 0) {
				
					movieName = [movieName stringByAppendingString:@"."];
				}
				movieName = [movieName stringByAppendingString:[parts objectAtIndex:i]];
			}
		}
	}
	
	return movieName;
}

- (NSString*)suggestedFilename {
	
	NSString* suggestedFilename = nil;
	
	if (_properties != nil) {
	
		[_properties stringValue:&suggestedFilename forKey:@"SuggestedMovieName" ofSection:@"Info"];
	}

	return suggestedFilename;
}


#pragma mark Accessors for the cutlist properties

- (int)cuts {

	int _cuts = 0;
	
	if (_properties != nil) {
	
		[_properties intValue:&_cuts forKey:@"NoOfCuts" ofSection:@"General"];
	}
	
	return _cuts;
}

- (void)startTimestamp:(double*)theStartTimestamp andDuration:(double*)theDuration ofCut:(int)theCut {

	if (_properties != nil) {
		
		[_properties doubleValue:theStartTimestamp forKey:@"Start" ofSection:[NSString stringWithFormat:@"Cut%d", theCut]];
		[_properties doubleValue:theDuration forKey:@"Duration" ofSection:[NSString stringWithFormat:@"Cut%d", theCut]];
	}
}


#pragma mark Accessors for the attributes

- (int)identifier {
	
    return _identifier;
}

- (void)setIdentifier:(int)value {
	
    if (_identifier != value) {
		
        _identifier = value;
    }
}

- (NSString*)name {
	
    return _name;
}

- (void)setName:(NSString*)value {
	
    if (_name != value) {
		
		[value retain];
        [_name release];
        _name = value;
    }
}

- (float)rating {
	
    return _rating;
}

- (void)setRating:(float)value {
	
    if (_rating != value) {
		
        _rating = value;
    }
}

- (int)ratingCount {
	
    return _ratingCount;
}

- (void)setRatingCount:(int)value {
	
    if (_ratingCount != value) {
		
        _ratingCount = value;
    }
}

- (NSString*)author {
	
    return _author;
}

- (void)setAuthor:(NSString*)value {
	
    if (_author != value) {
		
		[value retain];
        [_author release];
        _author = value;
    }
}

- (int)ratingByAuthor {
	
    return _ratingByAuthor;
}

- (void)setRatingByAuthor:(int)value {
	
    if (_ratingByAuthor != value) {
		
        _ratingByAuthor = value;
    }
}

- (NSString*)actualContent {
	
    return _actualContent;
}

- (void)setActualContent:(NSString*)value {
	
    if (_actualContent != value) {
		
		[value retain];
        [_actualContent release];
        _actualContent = value;
    }
}

- (NSString*)userComment {
	
    return _userComment;
}

- (void)setUserComment:(NSString*)value {
	
    if (_userComment != value) {
		
		[value retain];
        [_userComment release];
        _userComment = value;
    }
}

- (BOOL)selected {
	
	return _selected;
}

- (void)setSelected:(BOOL)flag {
	
	static BOOL sendingNotification = false;
	
	_selected = flag;
	
	if (!sendingNotification) {
		
		sendingNotification = YES;
		[[NSNotificationCenter defaultCenter] postNotificationName:CtCutlistSelectedNotification object:self];
		sendingNotification = NO;
	}
}

- (void)deselect {
	
	[self setSelected:NO];
}

- (void)select {
	
	[self setSelected:YES];
}

@end
