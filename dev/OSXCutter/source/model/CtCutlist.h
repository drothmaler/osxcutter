//
//  CtCutlist.h
//  OSXCutter
//
//  Created by Thomas Bonk on 08.06.07.
//  Copyright 2007 Thomas Bonk. All rights reserved.
//  
//  Creative Commons
//  Attribution-Noncommercial-Share Alike 2.0 Germany
//  You are free:
//      * to Share — to copy, distribute and transmit the work
//      * to Remix — to adapt the work
//  
//  Under the following conditions:
//  
//      * Attribution. You must attribute the work in the manner specified by 
//        the author or licensor (but not in any way that suggests that they 
//        endorse you or your use of the work).
//      * Noncommercial. You may not use this work for commercial purposes.
//      * Share Alike. If you alter, transform, or build upon this work, you may
//        distribute the resulting work only under the same or similar license 
//        to this one.
//      * For any reuse or distribution, you must make clear to others the 
//        license terms of this work. The best way to do this is with a link to 
//        this web page.
//      * Any of the above conditions can be waived if you get permission from 
//        the copyright holder.
//      * Nothing in this license impairs or restricts the author's moral rights.
//  
//  
//  Your fair dealing and other rights are in no way affected by the above.
//  This is a human-readable summary of the Legal Code (the full license):
//  http://creativecommons.org/licenses/by-nc-sa/2.0/de/legalcode
//


#import <Cocoa/Cocoa.h>


@class CFIniProperties;


// Notification that is being sent when the user selects another cutlist
// for cutting
#define CtCutlistSelectedNotification @"CtCutlistSelectedNotification"

// ID of a manually loaded cutlist
#define CtManualCutlistID -256


@interface CtCutlist : NSObject {

	int				 _identifier;
	NSString*		 _name;
	float			 _rating;
	int				 _ratingCount;
	NSString*		 _author;
	int				 _ratingByAuthor;
	NSString*		 _actualContent;
	NSString*		 _userComment;
	CFIniProperties* _properties;
	BOOL             _selected;
}

#pragma mark Construction/Destruction

+ (CtCutlist*)cutlist;
+ (CtCutlist*)cutlistWithContentsOfFile:(NSString*)filename;
- (id)init;
- (id)initWithContentsOfFile:(NSString*)filename;
- (void)dealloc;


#pragma mark Utility methods

- (NSComparisonResult)compareRating:(CtCutlist*)otherCutlist;
- (void)loadCutlistData:(NSError**)error;


#pragma mark Accessors for the cutlist properties

- (int)cuts;
- (void)startTimestamp:(double*)theStartTimestamp andDuration:(double*)theDuration ofCut:(int)theCut;


#pragma mark Accessors for special attributes

- (NSString*)movieName;
- (NSString*)suggestedFilename;


#pragma mark Accessors for the attributes

- (int)identifier;
- (void)setIdentifier:(int)value;

- (NSString*)name;
- (void)setName:(NSString*)value;

- (float)rating;
- (void)setRating:(float)value;

- (int)ratingCount;
- (void)setRatingCount:(int)value;

- (NSString*)author;
- (void)setAuthor:(NSString*)value;

- (int)ratingByAuthor;
- (void)setRatingByAuthor:(int)value;

- (NSString*)actualContent;
- (void)setActualContent:(NSString*)value;

- (NSString*)userComment;
- (void)setUserComment:(NSString*)value;

- (BOOL)selected;
- (void)setSelected:(BOOL)flag;
- (void)deselect;
- (void)select;

@end
