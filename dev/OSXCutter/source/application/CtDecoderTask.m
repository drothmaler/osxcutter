//
//  CtDecoderTask.m
//  OSXCutter
//
//  Created by Thomas Bonk on 07.06.07.
//  Copyright 2007 Thomas Bonk. All rights reserved.
//  
//  Creative Commons
//  Attribution-Noncommercial-Share Alike 2.0 Germany
//  You are free:
//      * to Share — to copy, distribute and transmit the work
//      * to Remix — to adapt the work
//  
//  Under the following conditions:
//  
//      * Attribution. You must attribute the work in the manner specified by 
//        the author or licensor (but not in any way that suggests that they 
//        endorse you or your use of the work).
//      * Noncommercial. You may not use this work for commercial purposes.
//      * Share Alike. If you alter, transform, or build upon this work, you may
//        distribute the resulting work only under the same or similar license 
//        to this one.
//      * For any reuse or distribution, you must make clear to others the 
//        license terms of this work. The best way to do this is with a link to 
//        this web page.
//      * Any of the above conditions can be waived if you get permission from 
//        the copyright holder.
//      * Nothing in this license impairs or restricts the author's moral rights.
//  
//  
//  Your fair dealing and other rights are in no way affected by the above.
//  This is a human-readable summary of the Legal Code (the full license):
//  http://creativecommons.org/licenses/by-nc-sa/2.0/de/legalcode
//

#import "CtCommon.h"
#import "CtDecoderTask.h"
#import "CtPreferencesModel.h"


// This is the path of the executable of otrdecoder
static NSString* _otrdecoderPath = nil;


@interface CtDecoderTask (private)

- (NSArray*)arguments;
- (void)decodingFinished;
- (void)getData:(NSNotification*)aNotification;

@end


@implementation CtDecoderTask

#pragma mark Initialization/Finalization

+ (void)initialize {

	if (_otrdecoderPath == nil) {
	
		_otrdecoderPath = [[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"otrdecoder"] retain];
	}
}

+ (CtDecoderTask*)decoderTask {

	CtDecoderTask* task = [[CtDecoderTask alloc] init];
	
	return [task autorelease];
}

- (id)init {

	if (self = [super init]) {

		_movie = nil;
		_target = nil;
		_task = [[NSTask alloc] init];
	}
	
	return self;
}

- (void)dealloc {
		
	[_task release];
	_task = nil;
	
	[super dealloc];
}


#pragma mark Executing otrdecoder

- (void)decode:(CtMovie*)aMovie andTrigger:(id<CtProgressProtocol>)aTarget; {
	
	_movie = aMovie;
	_target = aTarget;
	
    [_task setStandardOutput:[NSPipe pipe]];
    [_task setStandardError:[_task standardOutput]];
	
    [_task setLaunchPath:_otrdecoderPath];
    [_task setArguments:[self arguments]];
	
    // Here we register as an observer of the NSFileHandleReadCompletionNotification, which lets
    // us know when there is data waiting for us to grab it in the task's file handle (the pipe
    // to which we connected stdout and stderr above).  -getData: will be called when there
    // is data waiting.  The reason we need to do this is because if the file handle gets
    // filled up, the task will block waiting to send data and we'll never get anywhere.
    // So we have to keep reading data from the file handle as we go.
    [[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(getData:) 
												 name:NSFileHandleReadCompletionNotification 
											   object:[[_task standardOutput] fileHandleForReading]];
	
    // We tell the file handle to go ahead and read in the background asynchronously, and notify
    // us via the callback registered above when we signed up as an observer.  The file handle will
    // send a NSFileHandleReadCompletionNotification when it has data that is available.
    [[[_task standardOutput] fileHandleForReading] readInBackgroundAndNotify];
	
    // launch the task asynchronously
    [_task launch]; 
}

- (NSArray*)arguments {
	
	NSMutableArray* args = [NSMutableArray array];

	//[args addObject:[NSString stringWithFormat:@"-i %@", [_movie qualifiedFilename]]];
	[args addObject:@"-i"];
	[args addObject:[_movie qualifiedFilename]];
	
	NSMutableArray* pathComponents = [NSMutableArray arrayWithArray:[[_movie qualifiedFilename] pathComponents]];
	[pathComponents removeLastObject];
	[args addObject:@"-o"];
	[args addObject:[NSString pathWithComponents:pathComponents]];

	// TODO: Prüfen ob Email und Passwort überhaupt gesetzt sind.
	//[args addObject:[NSString stringWithFormat:@"-e %@", [[CtPreferencesModel instance] username]]];
	[args addObject:@"-e"];
	[args addObject:[[CtPreferencesModel instance] username]];
	//[args addObject:[NSString stringWithFormat:@"-p %@", [[CtPreferencesModel instance] password]]];
	[args addObject:@"-p"];
	[args addObject:[[CtPreferencesModel instance] password]];
	
	if (![[CtPreferencesModel instance] verifyOtrKeys]) {
		
		[args addObject:@"-q"];
	}
	
	if ([[CtPreferencesModel instance] forceOverwritingOfFiles]) {
		
		[args addObject:@"-f"];
	}
	
	[_target setCurrentCuttingStep:@"Decoding..."];
	[_target setCurrentProgress:0];
	[_target setMinCuttingProgress:0];
	[_target setMaxCuttingProgress:100];
	
	return [NSArray arrayWithArray:args];
}

- (void)decodingFinished:(BOOL)didSucceed {
    
	NSData *data = nil;
    
    // It is important to clean up after ourselves so that we don't leave potentially deallocated
    // objects as observers in the notification center; this can lead to crashes.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSFileHandleReadCompletionNotification object:[[_task standardOutput] fileHandleForReading]];
    
    // Make sure the task has actually stopped!
    [_task terminate];
	
	while ((data = [[[_task standardOutput] fileHandleForReading] availableData]) && [data length]) {
		
		//[controller appendOutput: [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease]];
	}
	
	// we tell the controller that we finished, via the callback, and then blow away our connection
	// to the controller.  NSTasks are one-shot (not for reuse), so we might as well be too.
	//[controller processFinished];
	//controller = nil;
	[_target decoder:self fireFinishedDecodingMovie:_movie succeeded:didSucceed];
	_target = nil;
	
	[_task terminate];
}

- (void)getData:(NSNotification*)aNotification {
	
	NSData *data = [[aNotification userInfo] objectForKey:NSFileHandleNotificationDataItem];
	
    // If the length of the data is zero, then the task is basically over - there is nothing
    // more to get from the handle so we may as well shut down.
    if ([data length]) {
		
		NSString*       progressStr  = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
		NSCharacterSet* digitsSet    = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
		NSString*       percent      = @"%";
		NSScanner*      scanner      = [NSScanner scannerWithString:progressStr];
		int             progress     = 0;
		
		[CtLogger info:@"otrdecoder output %@", progressStr];
		
		while ([scanner isAtEnd] == NO) {

			BOOL prefixDecoding = [progressStr hasPrefix:@"Decoding..."];
			BOOL rc             = YES;
			
			rc = rc && [scanner scanUpToCharactersFromSet:digitsSet intoString:NULL];
			rc = rc && ([scanner scanInt:&progress] || prefixDecoding);
			rc = rc && ([scanner scanString:percent intoString:NULL] || prefixDecoding);
			
			
			if (rc) {
			
				[_target setCurrentCuttingStep:[NSString stringWithFormat:@"Decoding... (%d%%)", progress]];
				[_target setCurrentProgress:progress];
				if (![_target progressingMovie:_movie]) {
				
					[self decodingFinished:YES];
				}
			}
			else {
			
				[CtLogger warn:@"Error while decoding %@", [_movie filename]];
				[self decodingFinished:NO];
			}
		}
        
    } 
	else {
		
        // We're finished here
        [self decodingFinished:YES];
    }
    
    // we need to schedule the file handle go read more data in the background again.
    [[aNotification object] readInBackgroundAndNotify]; 
}

@end
