//
//  CtApplicationController.h
//  OSXCutter
//
//  Created by Thomas Bonk on 06.06.07.
//  Copyright 2007 Thomas Bonk. All rights reserved.
//  
//  Creative Commons
//  Attribution-Noncommercial-Share Alike 2.0 Germany
//  You are free:
//      * to Share � to copy, distribute and transmit the work
//      * to Remix � to adapt the work
//  
//  Under the following conditions:
//  
//      * Attribution. You must attribute the work in the manner specified by 
//        the author or licensor (but not in any way that suggests that they 
//        endorse you or your use of the work).
//      * Noncommercial. You may not use this work for commercial purposes.
//      * Share Alike. If you alter, transform, or build upon this work, you may
//        distribute the resulting work only under the same or similar license 
//        to this one.
//      * For any reuse or distribution, you must make clear to others the 
//        license terms of this work. The best way to do this is with a link to 
//        this web page.
//      * Any of the above conditions can be waived if you get permission from 
//        the copyright holder.
//      * Nothing in this license impairs or restricts the author's moral rights.
//  
//  
//  Your fair dealing and other rights are in no way affected by the above.
//  This is a human-readable summary of the Legal Code (the full license):
//  http://creativecommons.org/licenses/by-nc-sa/2.0/de/legalcode
//

#import <Cocoa/Cocoa.h>
#import "CtMainWindow.h"
#import "CtMovieArrayController.h"
#import "CtProgressProtocol.h"


// Toolbar Item IDs
#define TBID_OPEN_FILE		1
#define TBID_DELETE_FILE	2
#define TBID_START_CUTTING	3
#define TBID_STOP_CUTTING	4
#define TBID_TOGGLE_DRAWER	5



@interface CtApplicationController : NSObject<CtProgressProtocol> {

	IBOutlet CtMainWindow*           _mainWindow;
	IBOutlet NSTableView*            _moviesTableView;
	IBOutlet NSTableView*            _cutlistsTableView;
	IBOutlet CtMovieArrayController* _movieArrayController;
	IBOutlet NSArrayController*      _cutlistsOfSelectedMovieController;
	IBOutlet NSPanel*                _progressPanel;
	IBOutlet NSProgressIndicator*    _progressIndicator;
	
	NSMutableArray* _movies;
	NSMutableArray* _cutlists;
	
	NSImage* _errorLoadingCutlistImage;
	NSImage* _noCutlistsImage;
	NSImage* _noErrorImage;
	NSImage* _notAMovieImage;
	
	BOOL _cuttingInProgress;
	BOOL _manualCutlistLoadingInProgress; 
	
	NSString* _currentCuttingMovie;
	NSString* _currentCuttingStep;
	long      _minCuttingProgress;
	long      _maxCuttingProgress;
	long      _currentProgress;
}

#pragma mark Construction/Destruction

- (id)init;
- (void)awakeFromNib;
- (void)dealloc;


#pragma mark Accessors for the attributes

- (NSString*)targetPath;
- (void)setTargetPath:(NSString*)value;
- (NSMutableArray*)movies;
- (NSMutableArray*)cutlists;

- (NSString*)currentCuttingMovie;
- (void)setCurrentCuttingMovie:(NSString*)value;
- (NSString*)currentCuttingStep;
- (void)setCurrentCuttingStep:(NSString*)value;
- (long)minCuttingProgress;
- (void)setMinCuttingProgress:(long)value;
- (long)maxCuttingProgress;
- (void)setMaxCuttingProgress:(long)value;
- (long)currentProgress;
- (void)setCurrentProgress:(long)value;


#pragma mark Action handlers

- (IBAction)doShowHelp:(id)sender;
- (IBAction)toggleCutlistDrawer:(id)sender;
- (IBAction)doDeleteFile:(id)sender;
- (IBAction)doStartCutting:(id)sender;
- (IBAction)doStopCutting:(id)sender;
- (IBAction)doOpenFile:(id)sender;
- (IBAction)doSelectTargetPath:(id)sender;
- (IBAction)doAddCutlist:(id)sender;


#pragma mark Delegate methods for table views

- (void)tableViewSelectionDidChange:(NSNotification*)aNotification;
- (void)tableView:(NSTableView*)aTableView willDisplayCell:(id)aCell forTableColumn:(NSTableColumn*)aTableColumn row:(int)rowIndex;


#pragma mark Delegate methods for the main window

- (BOOL)validateToolbarItem:(NSToolbarItem*)toolbarItem;


#pragma mark Delegate methods of the application

- (BOOL)application:(NSApplication*)theApplication openFile:(NSString*)filename;
- (void)application:(NSApplication*)sender openFiles:(NSArray*)filenames;
- (void)applicationDidFinishLaunching:(NSNotification*)aNotification;
- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication*)theApplication;

@end
