//
//  CtPreferencesModel.h
//  OSXCutter
//
//  Created by Thomas Bonk on 25.09.07.
//  Copyright 2007 Thomas Bonk. All rights reserved.
//  
//  Creative Commons
//  Attribution-Noncommercial-Share Alike 2.0 Germany
//  You are free:
//      * to Share — to copy, distribute and transmit the work
//      * to Remix — to adapt the work
//  
//  Under the following conditions:
//  
//      * Attribution. You must attribute the work in the manner specified by 
//        the author or licensor (but not in any way that suggests that they 
//        endorse you or your use of the work).
//      * Noncommercial. You may not use this work for commercial purposes.
//      * Share Alike. If you alter, transform, or build upon this work, you may
//        distribute the resulting work only under the same or similar license 
//        to this one.
//      * For any reuse or distribution, you must make clear to others the 
//        license terms of this work. The best way to do this is with a link to 
//        this web page.
//      * Any of the above conditions can be waived if you get permission from 
//        the copyright holder.
//      * Nothing in this license impairs or restricts the author's moral rights.
//  
//  
//  Your fair dealing and other rights are in no way affected by the above.
//  This is a human-readable summary of the Legal Code (the full license):
//  http://creativecommons.org/licenses/by-nc-sa/2.0/de/legalcode
//


#import <Cocoa/Cocoa.h>


@interface CtPreferencesModel : NSObject {

}

#pragma mark Initialization / Deallocation

+ (CtPreferencesModel*)instance;
+ (void)initialize;
- (id)init;
- (void)dealloc;


#pragma mark Accessors for the attributes

- (NSString*)targetPath;
- (void)setTargetPath:(NSString*)value;

- (BOOL)deleteOriginalFilesAfterCut;
- (void)setDeleteOriginalFilesAfterCut:(BOOL)value;

- (BOOL)exportExtAviToAvi;
- (void)setExportExtAviToAvi:(BOOL)value;

- (BOOL)exportExtHqAviToAvi;
- (void)setExportExtHqAviToAvi:(BOOL)value;

- (BOOL)exportExtMp4ToMp4;
- (void)setExportExtMp4ToMp4:(BOOL)value;

- (int)networkTimeout;
- (void)setNetworkTimeout:(int)timeout;

- (NSString*)username;
- (void)setUsername:(NSString*)newUsername;

- (NSString*)password;
- (void)setPassword:(NSString*)newPassword;

- (BOOL)verifyOtrKeys;
- (void)setVerifyOtrKeys:(BOOL)value;

- (BOOL)forceOverwritingOfFiles;
- (void)setForceOverwritingOfFiles:(BOOL)value;

- (BOOL)moveOtrKeysToTrash;
- (void)setMoveOtrKeysToTrash:(BOOL)value;

- (BOOL)loadCutlistsFromServer;
- (void)setLoadCutlistsFromServer:(BOOL)value;

- (NSString*)cutlistServer;
- (void)setCutlistServer:(NSString*)value;

- (BOOL)startCuttingAfterOpenFiles;
- (void)setStartCuttingAfterOpenFiles:(BOOL)value;

- (BOOL)quitAfterCutting;
- (void)setQuitAfterCutting:(BOOL)value;

@end
