//
//  CLNSLogAppender.m
//  CocoLogger
//
//  Created by Thomas Bonk on 27.09.06.
//  Copyright 2006 Thomas Bonk. All rights reserved.
//

#import "CLNSLogAppender.h"
#import "CLAppenderProtocol.h"


@implementation CLNSLogAppender

- (id)initWithParameters:(NSDictionary*)theParameters {
	
	self = [super init];
	
	return self;
}

- (void)dealloc {
	
	[super dealloc];
}

- (bool)open {
	
	return YES;
}

- (bool)close {
	
	return YES;
}

- (bool)flush {
	
	return YES;
}

- (bool)log:(NSString*)aMessage {

	NSLog( @"%@", aMessage );
	
	return YES;
}

@end
