//
//  CLCategory.h
//  CocoLogger
//
//  Created by Thomas Bonk on 06.09.06.
//  Copyright 2006 Thomas Bonk. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "CLPriorityLevel.h"
#import "CLAppenderProtocol.h"
#import "CLLayoutProtocol.h"


#define METHOD_ENTRY(logger)			[logger trace:@"Entering method %@", __PRETTY_FUNCTION__];
#define METHOD_EXIT(logger)				[logger trace:@"Exiting method %@", __PRETTY_FUNCTION__];
#define METHOD_RETURN(logger, retVal)	{ [logger trace:@"Exiting method %@", __PRETTY_FUNCTION__]; return retVal; }										



@interface CLCategory : NSObject {

	NSString*				_name;
	CLPriorityLevel			_priorityLevel;
	id<CLAppenderProtocol>	_appender;
	id<CLLayoutProtocol>	_layout;
}

#pragma mark Initialization / Deallocation

/** Creates and returns a category which is initialized with the default values.
 * \return A category initialized with the default values.
 */
+ (CLCategory*)category;

/** Initialize an instance of this class. The instance will be initialized
* with the dfeault values (nil, 0, etc.).
* \return The initialized instance.
*/
- (id)init;

/** Initialize an instance of this class with the given values.
 * \param aPriorityLevel The priority level of this log event.
 * \param anAppender The appender which handles the storing of the log events.
 * \param aLayout The layout that formats the log events.
 * \return The initialized instance.
 */
- (id)initWithName:(NSString*)aName 
	  priorityLeve:(CLPriorityLevel)aPriorityLevel 
		  appender:(id<CLAppenderProtocol>)anAppender 
			layout:(id<CLLayoutProtocol>)aLayout;

/** Deallocates the instance of this class.
 */
- (void)dealloc;


#pragma mark Logging methods

/** Logs a message with the given log level. The message will be logged if the
 * set log level is sufficient.
 * \param aLogLevel The log level of the message
 * \param aFormat The format of the log message
 * \param ... variable number of arguments 
 */
- (void)logWithLevel:(CLPriorityLevel)aLogLevel format:(NSString*)aFormat, ...;

// declare log methods for the priority levels
- (void)fatal:(NSString*)format, ...;
- (void)alert:(NSString*)format, ...;
- (void)crit:(NSString*)format, ...;
- (void)error:(NSString*)format, ...;
- (void)warn:(NSString*)format, ...;
- (void)notice:(NSString*)format, ...;
- (void)info:(NSString*)format, ...;
- (void)debug:(NSString*)format, ...;
- (void)trace:(NSString*)format, ...;


#pragma mark Setters / Getters

/** Gets the name of the category.
 * \return The name of teh category.
 */
- (NSString*)name;

/** Sets the name of the category.
 * \param aName The new name of the category.
 */
- (void)setName:(NSString*)aName;

/** Gets the priority level.
 * \return The priority level.
 */
- (CLPriorityLevel)priorityLevel;

/** Sets the priority level.
 * \param aPriorityLevel The new priority level.
 */
- (void)setPriorityLevel:(CLPriorityLevel)aPriorityLevel;

/** Gets the appender.
 * \return The appender.
 */
- (id<CLAppenderProtocol>)appender;

/** Sets the appender.
 * \param anAppender The new appender.
 */
- (void)setAppender:(id<CLAppenderProtocol>)anAppender;

/** Gets the layout.
 * \return The layout.
 */
- (id<CLLayoutProtocol>)layout;

/** Sets the layout.
 * \param aLayout The new layout.
 */
- (void)setLayout:(id<CLLayoutProtocol>)aLayout;

@end
