//
//  CLNotificationAppender.m
//  CocoLogger
//
//  Created by Thomas Bonk on 08.10.07.
//  Copyright 2007 Thomas Bonk. All rights reserved.
//

#import "CLNotificationAppender.h"


@implementation CLNotificationAppender

- (id)initWithParameters:(NSDictionary*)theParameters {
	
	if (self = [super init]) {
		
		_appenderKey = [theParameters objectForKey:@"AppenderKey"];
	}
	
	return self;
}

- (void)dealloc {
	
	[_appenderKey release];
	_appenderKey = nil;
	
	[super dealloc];
}

- (bool)open {
	
	return YES;
}

- (bool)close {
	
	return YES;
}

- (bool)flush {
	
	return YES;
}

- (bool)log:(NSString*)aMessage {
	
	NSDictionary* theUserInfo = [NSDictionary dictionaryWithObjectsAndKeys:_appenderKey, CLNotificationAppenderKey,
		                                                                   aMessage    , CLNotificationAppenderMessage,
		                                                                   nil];

	[[NSNotificationCenter defaultCenter] postNotificationName:CLAppenderNotificationName 
														object:self
													  userInfo:theUserInfo];
	
	return YES;
}

@end
