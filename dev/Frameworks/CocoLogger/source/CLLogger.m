//
//  CLLogger.m
//  CocoLogger
//
//  Created by Thomas Bonk on 28.09.06.
//  Copyright 2006 Thomas Bonk. All rights reserved.
//

#import "CLLogger.h"
#import "CLCategory.h"
#import "CLDefaultLayout.h"
#import "CLNSLogAppender.h"
#import "CLNotificationAPpender.h"
#import "CLPriorityLevel.h"

// a dictionary with all available layout classes
static NSMutableDictionary* _layoutClasses = nil;

// a dictionary with all available appender classes
static NSMutableDictionary* _appenderClasses = nil;

// the shared instance
static CLLogger* _sharedInstance = nil;


@implementation CLLogger

#pragma mark Creation

+ (void)initialize {

	_layoutClasses = [[NSMutableDictionary alloc] init];
	[_layoutClasses setValue:[CLDefaultLayout class] forKey:@"CLDefaultLayout"];
	
	_appenderClasses = [[NSMutableDictionary alloc] init];
	[_appenderClasses setValue:[CLNSLogAppender class] forKey:@"CLNSLogAppender"];
	[_appenderClasses setValue:[CLNotificationAppender class] forKey:@"CLNotificationAppender"];
}

+ (CLLogger*)sharedInstance {
	
	if( _sharedInstance == nil ) {
		
		_sharedInstance = [[CLLogger alloc] init];
	}
	
	return _sharedInstance;
}


- (id)init {

	if( _sharedInstance != nil ) {
		
		self = _sharedInstance;
	}
	else if( self = [super init] ) {
		
		_categories = [[NSMutableDictionary alloc] init];
	}
	
	return self;
}


- (void)dealloc {

	// does nothing...
}



#pragma mark Initialization


- (NSString*)resolveConfigPathForFilename:(NSString*)aConfigFilename {

	NSFileManager* fm  = [NSFileManager defaultManager];
	BOOL           isDir;
	NSString*      path;
	
	// Checker whether file is in /Library/Preferences/
	path = @"/Library/Preferences/";
	path = [path stringByAppendingPathComponent:aConfigFilename];
	if([fm fileExistsAtPath:path isDirectory:&isDir] && !isDir && [fm isReadableFileAtPath:path] ) {
		
		return path;
	}
	
	// Checker whether file is in ~/Library/Preferences/
	path = NSHomeDirectory();
	path = [path stringByAppendingPathComponent:@"/Library/Preferences/"];
	path = [path stringByAppendingPathComponent:aConfigFilename];
	if([fm fileExistsAtPath:path isDirectory:&isDir] && !isDir && [fm isReadableFileAtPath:path] ) {
		
		return path;
	}
	
	// Check whether file is in the main bundle.
	path = [[NSBundle mainBundle] resourcePath];
	path = [path stringByAppendingPathComponent:aConfigFilename];
	if([fm fileExistsAtPath:path isDirectory:&isDir] && !isDir && [fm isReadableFileAtPath:path] ) {
		
		return path;
	}
	
	return nil;
}


- (void)initializeLoggingWithConfigurationFile:(NSString*)aConfigFile {
	
	NSDictionary* theConfig = [NSDictionary dictionaryWithContentsOfFile:[self resolveConfigPathForFilename:aConfigFile]];
	
	[self initializeLoggingWithConfiguration:theConfig];
}


- (CLPriorityLevel)priorityLevelFromConfig:(NSDictionary*)aCategoryConfig {
	
	NSString* priorityAsString = [aCategoryConfig objectForKey:@"PriorityLevel"];
	
	return CLPriorityLevelFromName( priorityAsString );
}

- (id<CLAppenderProtocol>)appenderFromConfig:(NSDictionary*)aCategoryConfig {
	
	NSDictionary* appenderConfig     = [aCategoryConfig objectForKey:@"Appender"];
	NSString*     appenderClassName  = [appenderConfig objectForKey:@"Class"];
	NSDictionary* appenderParameters = [appenderConfig objectForKey:@"Parameters"];
	
	return [self appender:appenderClassName withParameters:appenderParameters];
}

- (id<CLLayoutProtocol>)layoutFromConfig:(NSDictionary*)aCategoryConfig {
	
	NSDictionary* layoutConfig     = [aCategoryConfig objectForKey:@"Layout"];
	NSString*     layoutClassName  = [layoutConfig objectForKey:@"Class"];
	NSDictionary* layoutParameters = [layoutConfig objectForKey:@"Parameters"];
	
	return [self layout:layoutClassName withParameters:layoutParameters];
}

- (void)initializeLoggingWithConfiguration:(NSDictionary*)aConfig {
	
	if( _categories == nil || [_categories count] == 0 ) {
		
		NSArray*      keys = [aConfig allKeys];
		NSEnumerator* e    = [keys objectEnumerator];
		NSString*     name = nil;
		
		while( name = [e nextObject] ) {
		
			CLCategory*     category = [self  categoryWithName:name];
			NSDictionary*   cfg      = [aConfig objectForKey:name];
			
			CLPriorityLevel        priorityLevel = [self priorityLevelFromConfig:cfg];
			id<CLAppenderProtocol> appender      = [self appenderFromConfig:cfg];
			id<CLLayoutProtocol>   layout        = [self layoutFromConfig:cfg];
			
			[category setPriorityLevel:priorityLevel];
			[category setAppender:appender];
			[category setLayout:layout];
		}
	}
}


#pragma Category related methods

- (CLCategory*)categoryWithName:(NSString*)aName {
	
	CLCategory* category = [_categories objectForKey:aName];
	
	if( category == nil ) {
		
		category = [CLCategory category];
		[category setName:aName];
		[_categories setValue:category forKey:aName];
	}
	
	return category;
}


#pragma Appender related methods

- (id<CLAppenderProtocol>)appender:(NSString*)anAppenderClassName 
					withParameters:(NSDictionary*)theAppenderParameters {
	
	Class                  appenderClass = [_appenderClasses objectForKey:anAppenderClassName];
	id<CLAppenderProtocol> appender      = [[[appenderClass alloc] initWithParameters:theAppenderParameters] autorelease];
	
	return appender;
}


#pragma Layout related methods

- (id<CLLayoutProtocol>)layout:(NSString*)aLayoutClassName 
				withParameters:(NSDictionary*)theLayoutParameters {
	
	Class                layoutClass = [_layoutClasses objectForKey:aLayoutClassName];
	id<CLLayoutProtocol> layout      = [[[layoutClass alloc] initWithParameters:theLayoutParameters] autorelease];
	
	return layout;
}

@end
