//
//  CLCategory.m
//  CocoLogger
//
//  Created by Thomas Bonk on 06.09.06.
//  Copyright 2006 Thomas Bonk. All rights reserved.
//

#import "CLCategory.h"
#import "CLLogEvent.h"


#define SHOULD_LOG(level) (_priorityLevel >= level)


@implementation CLCategory

#pragma mark Initialization / Deallocation

+ (CLCategory*)category {
	
	CLCategory* category = [[CLCategory alloc] init];
	
	return [category autorelease];
}

- (id)init {
	
    if (self = [super init]) {
		
		_name = nil;
        _priorityLevel = NOTSET;
        _appender = nil;
        _layout = nil;
    }
	
    return self;
}

- (id)initWithName:(NSString*)aName 
	  priorityLeve:(CLPriorityLevel)aPriorityLevel 
		  appender:(id<CLAppenderProtocol>)anAppender 
			layout:(id<CLLayoutProtocol>)aLayout {
	
	if (self = [super init]) {
		
		_name = [aName retain];
        _priorityLevel = aPriorityLevel;
        _appender = (id<CLAppenderProtocol>)[((NSObject*)anAppender) retain];
        _layout = (id<CLLayoutProtocol>)[((NSObject*)aLayout) retain];
    }
	
    return self;
}

- (void)dealloc {
	
    [self setAppender:nil];
    [self setLayout:nil];
    [super dealloc];
}



#pragma mark Logging methods

- (void)performLogWithLevel:(CLPriorityLevel)aLogLevel format:(NSString*)aFormat arguments:(va_list)argList {

	NSString*   message  = [[[NSString alloc] initWithFormat:aFormat arguments:argList] autorelease];
	CLLogEvent* logEvent = [CLLogEvent logEventWithCategory:[self name]
											  priorityLevel:aLogLevel 
													message:message 
													logDate:[NSDate date]];
	NSString*   logStr   = [[self layout] logEventToString:logEvent];
	
	[[self appender] log:logStr];
}


- (void)logWithLevel:(CLPriorityLevel)aLogLevel format:(NSString*)aFormat, ... {

	if( SHOULD_LOG(aLogLevel) ) {
		
		va_list argPtr;
		
		va_start(argPtr, aFormat);
		[self performLogWithLevel:aLogLevel format:aFormat arguments:argPtr];
		va_end(argPtr);
	}
}

// define the log methods
#define DEFINE_LOG_METHOD(name, level)											\
- (void)name:(NSString*)format, ... {											\
	if( SHOULD_LOG(level) ) {													\
		va_list argPtr;															\
		va_start(argPtr, format);												\
		[self performLogWithLevel:level format:format arguments:argPtr];	\
		va_end(argPtr);															\
	}																			\
}

DEFINE_LOG_METHOD(fatal, FATAL)
DEFINE_LOG_METHOD(alert, ALERT)
DEFINE_LOG_METHOD(crit, CRIT)
DEFINE_LOG_METHOD(error, ERROR)
DEFINE_LOG_METHOD(warn, WARN)
DEFINE_LOG_METHOD(notice, NOTICE)
DEFINE_LOG_METHOD(info, INFO)
DEFINE_LOG_METHOD(debug, DEBUG)
DEFINE_LOG_METHOD(trace, TRACE)



#pragma mark Setters / Getters

//=========================================================== 
//  name 
//=========================================================== 
- (NSString*)name {
	
    return _name; 
}

- (void)setName:(NSString*)aName {
	
    if (_name != aName) {
		
        [aName retain];
        [_name release];
        _name = aName;
    }
}

//=========================================================== 
//  priorityLevel 
//=========================================================== 
- (CLPriorityLevel)priorityLevel {
	
    return _priorityLevel;
}

- (void)setPriorityLevel:(CLPriorityLevel)aPriorityLevel {
	
    _priorityLevel = aPriorityLevel;
}

//=========================================================== 
//  appender 
//=========================================================== 
- (id<CLAppenderProtocol>)appender {
	
    return _appender; 
}

- (void)setAppender:(id<CLAppenderProtocol>)anAppender {
	
    if (_appender != anAppender) {
		
        [((NSObject*)anAppender) retain];
        [((NSObject*)_appender) release];
        _appender = anAppender;
    }
}

//=========================================================== 
//  layout 
//=========================================================== 
- (id<CLLayoutProtocol>)layout {
	
    return _layout; 
}

- (void)setLayout:(id<CLLayoutProtocol>)aLayout {
	
    if (_layout != aLayout) {
		
        [((NSObject*)aLayout) retain];
        [((NSObject*)_layout) release];
        _layout = aLayout;
    }
}

@end
