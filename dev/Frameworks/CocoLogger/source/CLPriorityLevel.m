//
//  CLPriorityLevel.m
//  CocoLogger
//
//  Created by Thomas Bonk on 29.08.06.
//  Copyright 2006 Thomas Bonk. All rights reserved.
//

#import "CLPriorityLevel.h"


static NSArray* names = nil;


NSString* CLPriorityName(CLPriorityLevel priorityLevel) {
	
	if( names == nil ) {
		
		names = [[NSArray arrayWithObjects:@"NOTSET", @"FATAL", 
					@"ALERT", @"CRITICAL", @"ERROR", @"WARN", 
					@"NOTICE", @"INFO", @"DEBUG", @"TRACE", nil] retain];
	}
	
	if( priorityLevel >= 1 && priorityLevel < [names count] ) {
		
		return [names objectAtIndex:priorityLevel];
	}
	else {
		
		return @"UNKNOWN";
	}
}

CLPriorityLevel CLPriorityLevelFromName(NSString* name) {
	
	unsigned priorityLevel;
	
	if( names == nil ) {
		
		names = [[NSArray arrayWithObjects:@"NOTSET", @"FATAL", 
			@"ALERT", @"CRITICAL", @"ERROR", @"WARN", 
			@"NOTICE", @"INFO", @"DEBUG", @"TRACE", nil] retain];
	}
	
	priorityLevel = [names indexOfObject:[name uppercaseString]];
	
	if( priorityLevel == NSNotFound ) {
		
		priorityLevel = NOTSET;
	}
	
	return priorityLevel;
}