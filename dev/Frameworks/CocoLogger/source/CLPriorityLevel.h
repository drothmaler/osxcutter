//
//  CLPriorityLevel.h
//  CocoLogger
//
//  Created by Thomas Bonk on 29.08.06.
//  Copyright 2006 Thomas Bonk. All rights reserved.
//

#import <Cocoa/Cocoa.h>


/** The priority levels. The priority of a category can be set to NOTSET, TRACE,
 *  DEBUG, INFO, NOTICE, WARN, ERROR, CRIT, ALERT or FATAL in ascending order of
 *  importance level of each message. 
 */
typedef enum {
	
	FATAL	= 1,
	ALERT	= 2,
	CRIT	= 3,
	ERROR	= 4, 
	WARN	= 5,
	NOTICE	= 6,
	INFO	= 7,
	DEBUG	= 8,
	TRACE	= 9,
	NOTSET  = 0
} CLPriorityLevel;


/** This function returns the name of the given priority level.
 *  \param priorityLevel The priority level for which the name will be returned.
 *  \return The name of the given priority level.
 */
NSString* CLPriorityName(CLPriorityLevel priorityLevel);

/** This function return the priority level for the given priority name.
 * If the name is unknown, NOTSET will be returned.
 * \param name The name of a pririoty level.
 * \return The priority level for a given name; NOTSET will be returned if the
 *         is unknown.
 */
CLPriorityLevel CLPriorityLevelFromName(NSString* name);
