//
//  CLLogEvent.h
//  CocoLogger
//
//  Created by Thomas Bonk on 03.09.06.
//  Copyright 2006 Thomas Bonk. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "CLPriorityLevel.h"


/** This class encapsulates a log event. It will be used to send the data that
 * will be logged to an appender and to a layout object.
 */
@interface CLLogEvent : NSObject {

	NSString*       _category;
	CLPriorityLevel _prioriyLevel;
	NSString*       _message;
	NSDate*			_logDate;
}

#pragma mark Initialization / Deallocation

/** Convenience method to create and initialize an instance of this class with 
 * the given values.
 * \param aCategory The category name.
 * \param aPriorityLevel The priority level of this log event.
 * \param aMessage The message that will be logged.
 * \param aLogDate The timestamp of this log event.
 * \return The initialized instance.
 */
+ (CLLogEvent*)logEventWithCategory:(NSString*)aCategory 
					  priorityLevel:(CLPriorityLevel)aPrioriyLevel 
							message:(NSString*)aMessage 
							logDate:(NSDate*)aLogDate;

/** Initialize an instance of this class. The instance will be initialized
 * with the dfeault values (nil, 0, etc.).
 * \return The initialized instance.
 */
- (id)init;

/** Initialize an instance of this class with the given values.
 * \param aCategory The category name.
 * \param aPriorityLevel The priority level of this log event.
 * \param aMessage The message that will be logged.
 * \param aLogDate The timestamp of this log event.
 * \return The initialized instance.
 */
- (id)initWithCategory:(NSString*)aCategory 
		 priorityLevel:(CLPriorityLevel)aPrioriyLevel 
			   message:(NSString*)aMessage 
			   logDate:(NSDate*)aLogDate;

/** Deallocates the instance of this class.
 */
- (void)dealloc;


#pragma mark Setters and Getters

/** Gets the category name.
 * \return The category name.
 */
- (NSString*)category;

/** Sets the category name.
 * \param aCatgeory The category name.
 */
- (void)setCategory:(NSString*)aCategory;

/** Gets the priority level.
 * \return The priority level.
 */
- (CLPriorityLevel)prioriyLevel;

/** Sets the priority level.
 * \param aPriorityLevel The priority level.
 */
- (void)setPrioriyLevel:(CLPriorityLevel)aPrioriyLevel;

/** Gets the log message.
 * \return The log message.
 */
- (NSString*)message;

/** Sets the log message.
 * \param aMessage The log message.
 */
- (void)setMessage:(NSString*)aMessage;

/** Gets the timestamp of this log event.
 * \return The timestamp of this log event.
 */
- (NSDate*)logDate;

/** Sets the timestamp of this log event.
 * \param aLogDate The timestamp of the log event.
 */
- (void)setLogDate:(NSDate*)aLogDate;

@end
