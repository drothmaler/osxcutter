//
// CLAppenderProtocol.h
// CocoLogger
//
// Created by Thomas Bonk on 30.08.06.
// Copyright 2006 Thomas Bonk. All rights reserved.
//

@protocol CLLayoutProtocol;

@class CLLogEvent;


/** This protocol provides the interface which must be provided by all appenders.
 *  An appender is an entity which is responsible for writing the log events to
 *  a stream or something like that.
 */
@protocol CLAppenderProtocol

/** This method initializes the appender with the given parameters.
 * \param theParameters The parameters for this object.
 * \return The initialized instance.
 */
- (id)initWithParameters:(NSDictionary*)theParameters;

/** Open the appender.
 *  \return true if opening the appender succeeded and false in case of an 
 *          error.
 */
- (bool)open;

/** Close the appender.
 *  \return true if closing the appender succeeded and false in case of an 
 *          error.
 */
- (bool)close;

/** Flush the appender.
 *  \return true if flushing the appender succeeded and false in case of an 
 *          error.
 */
- (bool)flush;

/** Write a log event. While writing the log event, the thread will be blocked.
 * \param aMessage The message as string that will be logged.
 */
- (bool)log:(NSString*)aMessage;

@end