//
//  NSMutableString_Extensions.m
//  CocoFoundation
//
//  Created by Thomas Bonk on 25.01.05.
//  Copyright 2005 Thomas Bonk. All rights reserved.
//

#import "CFMutableString_Extensions.h"


@implementation NSMutableString (CFMutableString)

- (void)trimLeft {
	
	int rightEnd = 0;
	
	while( rightEnd < [self length] && isspace([self characterAtIndex:rightEnd]) ) {
		
		rightEnd++;
	}

	[self deleteCharactersInRange:NSMakeRange(0, rightEnd)];
}

- (void)trimRight {
	
	int leftEnd = [self length]-1;
	
	while( leftEnd >= 0 && isspace( [self characterAtIndex:leftEnd] ) ) {
		
		   leftEnd--;
	}
		   
	[self deleteCharactersInRange:NSMakeRange(leftEnd+1, [self length]-leftEnd-1)];
}

- (void)trim {
	
	[self trimLeft];
	[self trimRight];
}

@end
