//
//  CFMutableString_Extensions.h
//  CocoFoundation
//
//  Created by Thomas Bonk on 25.01.05.
//  Copyright 2005 Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
@abstract Extensions to NSMutablString.
@version 1.0
*/
@interface NSMutableString (CFMutableString)

/*!
@abstract Trim this string on the left side, i.e. remove the whitespaces.
*/
- (void)trimLeft;

/*!
@abstract Trim this string on the right side, i.e. remove the whitespaces.
*/
- (void)trimRight;

/*!
@abstract Trim this string, i.e. remove the whitespaces on the left and right side.
*/
- (void)trim;
@end
