//
//  CFArray_Extensions.h
//
//  Copyright (c) 2001-2002, Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (CFArray)
- (BOOL)containsObjectIdenticalTo: (id)object;
@end

@interface NSMutableArray (CFMutableArray)
- (void) insertObjectsFromArray:(NSArray *)array atIndex:(int)index;
@end

