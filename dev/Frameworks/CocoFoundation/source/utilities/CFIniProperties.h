//
//  CFIniProperties.h
//
//  Created by Thomas Bonk on 08.06.07.
//  Copyright 2007 Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CFIniProperties : NSObject {

	NSMutableDictionary* _sections;
}

#pragma mark Convenience methods

+ (CFIniProperties*)IniFile;
+ (CFIniProperties*)IniFileWithData:(NSData*)theData;


#pragma mark Construction/Destruction

- (id)init;
- (id)initWithData:(NSData*)theData;
- (void)dealloc;


#pragma mark Getting and setting data

- (void)addSection:(NSString*)aSection;
- (NSArray*)sections;
- (void)addKey:(NSString*)aKey andValue:(NSString*)aValue toSection:(NSString*)aSection;

- (BOOL)stringValue:(NSString**)aString forKey:(NSString*)aKey ofSection:(NSString*)aSection;
- (BOOL)intValue:(int*)anInt forKey:(NSString*)aKey ofSection:(NSString*)aSection;
- (BOOL)doubleValue:(double*)aDouble forKey:(NSString*)aKey ofSection:(NSString*)aSection;

@end
