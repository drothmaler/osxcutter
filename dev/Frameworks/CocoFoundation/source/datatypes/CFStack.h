//
//  CFStack.h
//  CocoFoundation
//
//  Created by Thomas Bonk on 18.01.05.
//  Copyright 2005 Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>


/*!
@abstract This class provides an object-oriented interface to a stack.
@version 1.0
 */
@interface CFStack : NSObject {
	
	// @! \brief array that holds the stack elements
	NSMutableArray* mutableArray;
}

#pragma mark Initialization / destruction

/*!
@abstract Convenience method that creates a instance of this class.
@result A stack object.
*/
+ (CFStack*)stack;

/*!
@abstract Initializer
*/
- (id)init;

/*!
@abstract Deallocator
*/
- (void)dealloc;


#pragma mark Operations

/*!
@abstract Returns the stack size
@result The stack size.
*/
- (int)size;

/*!
@abstract This method pushed an object unto the stack.
@param anObject The object that will be pushed to the stack.
*/
- (void)push:(id)anObject;

/*!
@abstract This method pops the topmost object off the stack and returns it.
@result The object from the top of the stack.
*/
- (id)pop;

/*!
@abstract This method returns the topmost object of the stack.
@result The object from the top of the stack.
*/
- (id)topOfStack;

@end
